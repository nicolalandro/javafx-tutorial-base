# JavaFX base

## Preparare Eclipse
* [youtube video](https://www.youtube.com/watch?v=bC4XB6JAaoU)
* installare da eclipse marketplace la libreria specifica
* scaricare i [jar](https://gluonhq.com/products/javafx/) 
  * e settarle come librerie utente di modo da importarle facilmente nei progetto
* importare le librerie nel classpath
* settare nelle run config il vm args con il path delle librerie 
  * "--module-path "/home/mint/Apps/javafx-sdk-15.0.1/lib" --add-modules javafx.controls,javafx.fxml"
  
## Scene Builder
* [download](https://gluonhq.com/products/scene-builder/) la versione per il tuo os
* usalo per generare e modificare i tuoi fxml  
  
## Esempio base (più simile a swing)
* src/application/Main.java
* click button -> print in console

## Esempio base con FXML
* src/apllication/MainFXML.java
  * controller: HomeController.java
  * view: main.fxml
  * style: application.css
* click button -> print in console e visualizza risultato su label 

# Esercizi

## Calcolatrice
* src/calculator/Main.java
  * controller: CalculatorController.java
  * view: main.fxml
  * style: -

## Macchinetta delle bevande
* src/distributore/Main.java
  * controller: DistributoreController.java
  * view: main.fxml
  * style: -

## Cassa automatica
* src/cassautomatica/Main.java
  * controller: CassaController.java
  * view: main.fxml
  * model: Product.java
  * style: -

## Tutorial & altre referenze
* [get started](https://docs.oracle.com/javase/8/javafx/get-started-tutorial/get_start_apps.htm)
* [more complete slides](https://slideplayer.com/slide/16543324/)
* [youtube tutorial](https://www.youtube.com/watch?v=FLkOX4Eez6o&list=PL6gx4Cwl9DGBzfXLWLSYVy8EbTdpGbUIG)
* hai problemi? Cerca su [stackoverflow](https://stackoverflow.com/)


