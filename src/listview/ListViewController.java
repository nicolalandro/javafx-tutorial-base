package listview;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

public class ListViewController implements Initializable {
	@FXML
	ListView<String> mListView;
	ObservableList<String> observableItems;
	ArrayList<String> arrayList = new ArrayList<String>();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		arrayList.add("default item");
		observableItems = FXCollections.observableList(arrayList);
		this.mListView.setItems(observableItems);
	}

	public void onAdd() {
		this.mListView.getItems().add("Item ");
	}
	
	public void onRemove() {
		ObservableList<Integer> indices = this.mListView.getSelectionModel().getSelectedIndices();
		for (Integer index : indices) {
			this.mListView.getItems().remove((int) index);
		}
	}

}
