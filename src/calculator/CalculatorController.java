package calculator;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class CalculatorController implements Initializable {
	@FXML
	TextField mDisplay;

	@FXML
	CheckBox mBoldCheckBox;

	@FXML
	CheckBox mItalicCheckBox;
	
	@FXML
	ColorPicker mColorPicker;

	boolean isBold = false;
	boolean isItalic = false;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mColorPicker.setValue(Color.BLACK);
		this.styleChange();
	}

	public void onClickQuit() {
		Platform.exit();
	}

	public void onClickNumber(ActionEvent event) {
		Button button = (Button) event.getSource();
		String buttonValue = button.getText();
		this.mDisplay.setText(this.mDisplay.getText() + buttonValue);
	}

	public void onClickReset() {
		this.mDisplay.setText("");
	}

	public void onClickCompute() {
		try {
			String text = this.mDisplay.getText();
			String[] stringNumbers = text.split("\\+");
			Integer sumValue = 0;
			for (String s : stringNumbers) {
				Integer val = Integer.parseInt(s);
				sumValue += val;
			}
			this.mDisplay.setText(sumValue.toString());
		} catch (Exception e) {
			this.mDisplay.setText("ERROR!");
			System.out.println(e.toString());
		}
	}

	public void onCheckBold() {
		this.isBold = this.mBoldCheckBox.isSelected();
		this.styleChange();
	}

	public void onCheckItalic() {
		this.isItalic = this.mItalicCheckBox.isSelected();
		this.styleChange();
	}

	public void styleChange() {
		String cssString = "";
		if (this.isBold) {
			cssString += "-fx-font-weight: bold;";
		}
		if (this.isItalic) {
			cssString += "-fx-font-posture: italic;";
		}
		cssString += "-fx-text-fill: #" + mColorPicker.getValue().toString().substring(2) + ";";
		System.out.println(cssString);
		this.mDisplay.setStyle(cssString);
//	    Font font = Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 12);
//		this.mDisplay.setFont(font);
	}

}
