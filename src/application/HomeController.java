package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class HomeController implements Initializable {
	@FXML
	Label mLabel;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public void onButtonClick() {
		System.out.println("Hello World!");
		mLabel.setText("Hello World!");
	}
}
