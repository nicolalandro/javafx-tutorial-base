package application;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
public class MainFXML extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = getParentFromFxml("src/application/main.fxml");

			Scene scene = new Scene(root, 400, 400);

			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Parent getParentFromFxml(String resourcePath) throws IOException {
		URL fxmlUrl = new File(resourcePath).toURI().toURL();
		System.out.println(fxmlUrl.toString());
		FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
//		fxmlLoader.setController(new HomeController());
		return fxmlLoader.load();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
