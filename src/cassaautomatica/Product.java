package cassaautomatica;

public class Product {
	private String description;
	private String barcode;
	private Integer price;
	
	public Product(String barcode, String description, Integer price) {
		this.barcode = barcode;
		this.description = description;
		this.price = price;
	}
	
	public String toString() {
		return "Barcode: " + this.barcode + ", description: " + this.description + ", price: " + this.price.toString();
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	
}
