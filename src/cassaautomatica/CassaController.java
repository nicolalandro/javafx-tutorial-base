package cassaautomatica;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CassaController implements Initializable {

	private HashMap<String, Product> products = new HashMap<String, Product>();

	@FXML
	ListView<String> mProductListView;
	ArrayList<String> productsStrings = new ArrayList<String>();
	ObservableList<String> viewProductsItems;

	@FXML
	ListView<String> mCartProductListView;

	@FXML
	TextField mBarcodeTextField;
	@FXML
	Spinner mSpinner;
	
	ArrayList<Integer> prices = new ArrayList<>();
	@FXML
	Label mTotalLabel;
	
	@FXML
	Text mDisplay;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		products.put("1", new Product("1", "Glass of water", 2));
		products.put("2", new Product("2", "Bread", 3));
		products.put("3", new Product("3", "Duff", 7));
		products.put("4", new Product("4", "Pasta", 4));
		products.put("5", new Product("5", "Vegetables", 1));


		for (Entry<String, Product> p : products.entrySet()) {
			productsStrings.add(p.getValue().toString());
		}

		viewProductsItems = FXCollections.observableList(productsStrings);
		this.mProductListView.setItems(viewProductsItems);

		mSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100));
	}

	public void addProductToCart() {
		Integer quantity = (Integer) mSpinner.getValue();
		String barcode = mBarcodeTextField.getText();
		if (products.containsKey(barcode)) {
			Product p = products.get(barcode);
			int price = p.getPrice() * quantity;
			
			String visualized = "Barcode: " + p.getBarcode();
			visualized+= ", description: " + p.getDescription();
			visualized += " (x" + quantity.toString() + ")";
			visualized += ", price: " + Integer.toString(price);
			
			this.mCartProductListView.getItems().add(visualized);
			
			this.prices.add(price);
			
			this.mDisplay.setText("Product added");
		} else {
			this.mDisplay.setText("The Product does not exist");
		}
		updatePrice();
	}

	public void removeProductFromCart() {
		ObservableList<Integer> indices = this.mCartProductListView.getSelectionModel().getSelectedIndices();
		System.out.println(indices.size());
		for (Integer index : indices) {
			this.mCartProductListView.getItems().remove((int) index);
			this.prices.remove((int) index);
			this.mCartProductListView.getSelectionModel().select(-1);
//			System.out.println("list size : " + this.mCartProductListView.getItems().size());
		}
		updatePrice();
		this.mDisplay.setText("Product removed");
	}
	
	public void updatePrice() {
		Integer price = 0;
		for(Integer p : this.prices) {
			price += p;
		}
		
		this.mTotalLabel.setText("Total: " + price.toString());
	}
	
	public void clickPayButton() {
		this.mCartProductListView.getItems().clear();
		this.prices.clear();
		this.updatePrice();
		this.mDisplay.setText("Thank you!");
	}

}
