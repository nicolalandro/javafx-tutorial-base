package distributore;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class DistributoreController implements Initializable {
	@FXML
	TextField mDisplay;
	@FXML
	RadioButton mCaffeRadioButton;
	@FXML
	RadioButton mCioccolataRadioButton;
	@FXML
	RadioButton mTeRadioButton;
	@FXML
	RadioButton mCappuccinoRadioButton;

	ToggleGroup selectionToggleGroup = new ToggleGroup();

	@FXML
	RadioButton mNoSugarRadioButton;
	@FXML
	RadioButton mSugarRadioButton;
	@FXML
	RadioButton mMoreSugarRadioButton;

	ToggleGroup sugarToggleGroup = new ToggleGroup();

	@FXML
	VBox mMachineVBox;
	@FXML
	VBox mLoginVBox;

	@FXML
	Text mLogText;

	@FXML
	TextField mUserName;
	@FXML
	PasswordField mUserPassword;
	@FXML
	MenuItem mCoinsMenuItem;
	@FXML
	Spinner mSpinner;

	boolean isLogged = false;
	HashMap<String, String> users = new HashMap<>();
	HashMap<String, Integer> usersCoins = new HashMap<>();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		mCaffeRadioButton.setToggleGroup(selectionToggleGroup);
		mCioccolataRadioButton.setToggleGroup(selectionToggleGroup);
		mTeRadioButton.setToggleGroup(selectionToggleGroup);
		mCappuccinoRadioButton.setToggleGroup(selectionToggleGroup);

		selectionToggleGroup.selectedToggleProperty()
				.addListener((observable, oldVal, newVal) -> this.showPrice(newVal));

		mNoSugarRadioButton.setToggleGroup(sugarToggleGroup);
		mSugarRadioButton.setToggleGroup(sugarToggleGroup);
		mMoreSugarRadioButton.setToggleGroup(sugarToggleGroup);

		mNoSugarRadioButton.setSelected(true);

		mSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100));
		mSpinner.setVisible(false);
	}
	
	public void onClickQuit() {
		Platform.exit();
	}

	public void showPrice(Toggle newVal) {
		RadioButton selected = (RadioButton) newVal;
		int price = 0;
		switch (selected.getText()) {
		case "Caffe":
			price = 2;
			break;
		case "Cioccolata":
			price = 5;
			break;
		case "Te":
			price = 3;
			break;
		case "Cappuccino":
			price = 6;
			break;
		}
		mDisplay.setText(Integer.toString(price));
	}

	public void paid() {
		if (mDisplay.getText().isEmpty()) {
			this.mLogText.setText("Select something");
		} else {
			mDisplay.setText("");
		}
	}

	public void onClickLoginLogout() {
		if (this.isLogged) {
			this.isLogged = false;
			mCoinsMenuItem.setText("Coins");
			mSpinner.setVisible(false);
			mLogText.setText("Not logged");
		} else {
			showLogin();
		}

	}

	public void showLogin() {
		mMachineVBox.setVisible(false);
		mLoginVBox.setVisible(true);
	}

	public void showMachine() {
		mMachineVBox.setVisible(true);
		mLoginVBox.setVisible(false);
	}

	public void onClickRegister() {
		String name = this.mUserName.getText();
		if (this.users.containsKey(name)) {
			mLogText.setText("User Already Registered");
		} else {
			String password = this.mUserPassword.getText();
			users.put(name, password);
			usersCoins.put(name, 0);
			mLogText.setText("User Registered");
			showMachine();
		}
	}

	public void onClickLogin() {
		String name = this.mUserName.getText();
		String password = this.mUserPassword.getText();
		if (this.users.containsKey(name)) {
			if (password.equals(this.users.get(name))) {
				isLogged = true;
				mCoinsMenuItem.setText("Coins: " + usersCoins.get(name).toString());
				mLogText.setText("Logged");
				mSpinner.setVisible(true);
				showMachine();
			} else {
				mLogText.setText("Wrong password");
			}
		} else {
			mLogText.setText("User does not exist");
		}
	}

	public void insertCoins() {
		if (this.isLogged) {
			String name = this.mUserName.getText();
			Integer val = (Integer) this.mSpinner.getValue();
			usersCoins.put(name, usersCoins.get(name) + val);
			mCoinsMenuItem.setText("Coins: " + usersCoins.get(name).toString());
		}
	}

	public void onClickCreditCoins() {
		if (this.isLogged) {
			String priceString = this.mDisplay.getText();
			if (priceString.isEmpty()) {
				this.paid();
			} else {
				String name = this.mUserName.getText();
				Integer price = Integer.parseInt(priceString);
				Integer user_coins = usersCoins.get(name) - price;
				if (user_coins < 0) {
					mLogText.setText("No enought coins");
				} else {
					usersCoins.put(name, user_coins);
					mCoinsMenuItem.setText("Coins: " + usersCoins.get(name).toString());
					this.paid();
				}
			}
		} else {
			this.mLogText.setText("You are not logged");
			this.showLogin();
		}
	}
}
