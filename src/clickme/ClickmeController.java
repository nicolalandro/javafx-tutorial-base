package clickme;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

public class ClickmeController implements Initializable {
	@FXML
	Text mText;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public void onButtonClick() {
		this.mText.setText("Hello World!");
	}
}
