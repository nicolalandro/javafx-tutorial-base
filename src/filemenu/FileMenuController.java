package filemenu;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

public class FileMenuController implements Initializable {
	@FXML
	Label mText;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public void onClickQuit() {
		Platform.exit();
	}

	public void onMenuItemClick(ActionEvent event) {
		MenuItem menuitem = (MenuItem) event.getSource();
		this.mText.setText(menuitem.getText());
	}
}
